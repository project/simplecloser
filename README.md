#CONTENTS OF THIS FILE#

- Introduction
- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers

##INTRODUCTION##

Selector SimpleCloser uses jQuery (to work on iOS/touch/mobile, you'll want
to run jQuery Update) to hide elements when another area of the page outside
of the listed element is clicked.

The admin page allows you to select the elements using jQuery compatible
selectors.

What I created this module for was to hide the Ajax results of a Views Filter
that I was using as a site search. Another use case could be to disregard
information modal dialogs.

For a full description of the module, visit the project page:
https://www.drupal.org/sandbox/elliotc/2587553

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/2587553

##REQUIREMENTS##

No special requirements

##RECOMMENDED MODULES##

No other modules are required. However, you'll want jQuery Update if you
want this to work with touchscreen devices (iOS/Android, etc.).

jQuery Update 7.x-3.x
https://www.drupal.org/project/jquery_update

Also, this was tested using
Omega Theme 7.x-3.1
https://www.drupal.org/project/omega

It's possible that other themes could create some unintended consequences.

##INSTALLATION##

Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

##CONFIGURATION##

Use the config page at /admin/config/user-interface/simplecloser
to select the element(s) you would like dynamically hidden by the module.

For example:

   #main-menu would select the default navigation menu in Omega 3 theme.

##TROUBLESHOOTING##

Make certain that you are using jQuery compatible selectors
https://api.jquery.com/category/selectors/

##FAQ##

Q: Why would you not use some sort of close gadget instead?
A: That seemed like more work when I believe the average user either won't
   care about the element remaining on the screen or will click elsewhere.

Q: How do I get the element back after it's closed/hidden?
A: You can't. You'd need to reload the page or repeat the action that
   created the element in the first place. It would be weird to click
  "anywhere" as a toggle for the element for the use case I was going after.

Q: What the heck!? This doesn't work AT ALL with my iPhone! Why?
A: Make certain to run jQuery Update (link above) if you want it to work
   on touchscreen devices. jQuery 1.7+ is needed, and Drupal 7 only has
   jQuery 1.5.

##MAINTAINERS##

Current maintainers:

- Elliot Christenson (elliotc) - https://www.drupal.org/u/elliotc
